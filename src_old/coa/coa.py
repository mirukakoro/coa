from textx import metamodel_from_file
import json
import logging
import datetime

__version__ = '0.0.0-a'


class program():
  def __init__(self, log_level=logging.DEBUG,
               log_path='coa-{}.log'.format(datetime.datetime.now().strftime('%Y-%m-%d %H-%M-%S'))):
    self.config = {}
    with open('config.json') as file:
      self.config = json.load(file)
    with open('template.go') as file:
      self.go_fmt = file.read().replace('<ver>', __version__).replace('<pkg_name>', 'main')
    self.callee_conv = self.config['cvt_go']['callee']
    self.definecall_hst = []
    self.cvt_go_lib = []
    self.cvt_go_import_fmt = 'import "{}"'
    # log_path = None
    logging.basicConfig(
      format = '%(asctime)s %(levelname)-8s %(message)s',
      level = log_level,
      filename = log_path,
      datefmt = '%Y-%m-%d %H:%M:%S (%z)')
    logging.info('Init program class.')
  
  def _cvt_go_cont(self, cont):
    logging.debug('_cvt_go_cont {}'.format(cont))
    result = ''
    for i in range(len(cont)):
      obj = cont[i]
      this_r = self._cvt_go_object(obj)  # THIS Result
      if i == len(cont) - 1:
        result += '{}'.format(this_r)
      else:
        result += '{}, '.format(this_r)
    return result
  
  def _cvt_go_callee_conv_helper(self, i, callee, callee_conv):
    logging.debug('_cvt_go_evokecall_helper {} {} {}'.format(i, callee, callee_conv))
    if isinstance(callee_conv, dict):
      if callee[i] in callee_conv.keys():
        return self._cvt_go_callee_conv_helper(i + 1, callee, callee_conv[callee[i]])
      else:
        return [callee[0], None]#self._cvt_go_callee_conv_helper(i + 1, callee, callee[i])
    else:
      return callee_conv
  
  def _cvt_go_callee_conv(self, raw_callee):
    logging.debug('_cvt_go_callee_conv in {}'.format(raw_callee))
    callee = ['{}{}'.format(raw_callee[0], raw_callee[1])]
    for i in range(len(raw_callee[2:])):
      callee.append('.{}'.format(raw_callee[i+2]))
    result = self._cvt_go_callee_conv_helper(0, callee, self.callee_conv)
    logging.debug('_cvt_go_callee_conv out {}'.format(raw_callee))
    return result
  
  def _cvt_go_evokecall_control(self, callee, args, has_suffix=False):
    logging.debug('_cvt_go_evokecall_control {}'.format(callee, args))
    if callee in ('.else'):
      cond_result   = ''
      evoker = {'type': 'EvokeCall', 'data': [args[0]['data'], []]}
      code_result   = self._cvt_go_object(evoker)
    else:
      cond_result = self._cvt_go_object(args[0])
      evoker = {'type': 'EvokeCall', 'data': [args[1]['data'], []]}
      code_result = self._cvt_go_object(evoker)
    evokecall_fmt = '<callee><cond>{<code>};'  # EVOKECALL ForMaT
    # evokecall_result = evokecall_fmt.format('<callee>', callee).replace('<cond>', cond_result).replace('<code>', code_result)
    name = {'.if':'if', '.elif':'else if', '.else':'else', '.else_if':'else if',
            'if':'if', 'elif':'else if', 'else':'else', 'else_if':'else if',
            '.while':'for', 'while':'for', }
    callee = name[callee]
    suffix = {True: ' ', False: ';'}[has_suffix]
    if callee == 'else':
      evokecall_result = callee + ' {' + code_result + '};' # else controls don't have suffixes
    else:
      evokecall_result = callee + ' ' + cond_result + ' {' + code_result + '}' + suffix
    return evokecall_result
  
  def _cvt_go_escape(self, inp):
    return inp#.replace('coaInternal', 'coaFakeInternal')

  def _cvt_go_unescape(self, inp):
    return inp#.replace('coaFakeInternal', 'coaInternal')
  
  def _cvt_go_evokecall_native(self, callee, args, has_suffix = False):
    logging.debug('_cvt_go_evokecall_native in {} {} {}'.format(callee, args, has_suffix))
    if callee in ('.run', 'run'):
      evoker = args[0]['data']
      code_result = self._cvt_go_object(evoker)[1:-1]
    else:
      logging.critical('_cvt_go_evokecall_native called with non-supported callees')
      return '/*coa critical error (see log): _cvt_go_evokecall_native called with non-supported callees*/'
    suffix = {True: ' ', False: ';'}[has_suffix]
    evokecall_result = code_result + suffix
    logging.debug('_cvt_go_evokecall_native out {}'.format(evokecall_result))
    return evokecall_result
    
  def _cvt_go_evokecall_import(self, callee, args, has_suffix = False):
    logging.debug('_cvt_go_evokecall_import in {} {} {}'.format(callee, args, has_suffix))
    if callee in ('.import', 'import'):
      evoker = args[0]['data']
      code_result = self._cvt_go_object(evoker)[1:-1]
      self.cvt_go_lib.append(code_result)
      result = ''
    else:
      logging.critical('_cvt_go_evokecall_import called with non-supported callees: \'{}\''.format(callee))
      logging.debug('_cvt_go_evokecall_native out critical error')
      result = '/*coa critical error (see log): _cvt_go_evokecall_native called with non-supported callees*/'
    logging.debug('_cvt_go_evokecall_native out {}'.format(result))
    return result
  
  def _cvt_go_evokecall(self, command, has_suffix=False):
    for line in json.dumps(command).split('\n'):
      logging.debug('_cvt_go_evokecall {}'.format(line))
    splitter = {'.if': self._cvt_go_evokecall_control,
                '.elif': self._cvt_go_evokecall_control,
                '.else_if': self._cvt_go_evokecall_control,
                '.else': self._cvt_go_evokecall_control,
                'if': self._cvt_go_evokecall_control,
                'elif': self._cvt_go_evokecall_control,
                'else_if': self._cvt_go_evokecall_control,
                'else': self._cvt_go_evokecall_control,
                'while': self._cvt_go_evokecall_control,
                '.while': self._cvt_go_evokecall_control,
                '.run': self._cvt_go_evokecall_native,
                'run': self._cvt_go_evokecall_native,
                '.import': self._cvt_go_evokecall_import,
                'import': self._cvt_go_evokecall_import, }
    callee = command['data'][0]
    args = command['data'][1]
    callee_raw    = self._cvt_go_callee_conv(callee['data'])
    callee_result = callee_raw[0]
    if callee_result in splitter.keys():
      return splitter[callee_result](callee_result, args, has_suffix)
    callee_lib    = callee_raw[1]
    if not not callee_lib and callee_lib not in self.cvt_go_lib:
      self.cvt_go_lib.append(callee_lib)
    args_result = self._cvt_go_cont(args)
    evokecall_fmt = '{callee}({args});' # EVOKECALL ForMaT
    evokecall_result = evokecall_fmt.format(callee = callee_result, args = args_result)
    return evokecall_result
  
  def _cvt_go_definecall(self, command, has_suffix=False):
    logging.debug('_cvt_go_definecall in {}'.format(command))
    definecall_fmt = '{defined} {operator} {definer};'
    defined = self._cvt_go_escape(self._cvt_go_object(command['data'][0]))
    if defined in self.definecall_hst:
      operator = '='
    else:
      operator = ':='
    self.definecall_hst.append('{defined}'.format(defined = defined))
    definer = self._cvt_go_escape(self._cvt_go_object(command['data'][1]))
    result = definecall_fmt.format(defined = defined, operator = operator,
                                   definer = definer)
    logging.debug('_cvt_go_definecall out {}'.format(result))
    return result
  
  def _cvt_go_func_args(self, cont):
    logging.debug('_cvt_go_evokecall_args in {}'.format(cont))
    result = ''
    for i in range(len(cont)):
      obj = cont[i]
      this_r = self._cvt_go_object(obj)  # THIS Result
      result += '{} interface{}'.format(this_r, '{}')
      if i != len(cont) - 1:
        result += ', '
    logging.debug('_cvt_go_evokecall_args out {}'.format(result))
    return result
  
  def _cvt_go_func(self, command):
    logging.debug('_cvt_go_func {}'.format(command))
    args = command[0]
    code = command[1]
    func_fmt = '''func(<args>){<code>}'''
    line_fmt = '''{code}'''
    code_r = ''
    for i in range(len(code)):
      line_data = self._cvt_go_object(code[i])
      code_r += line_fmt.format(code = line_data)
    result = func_fmt.replace('<args>', self._cvt_go_func_args(args)).replace('<code>', code_r)
    return result
  
  def _cvt_go_object(self, command, has_suffix=False):
    logging.debug('_cvt_go_object in {} {}'.format(str(command), str(has_suffix)))
    type_name = command['type']
    if type_name == 'Bool':
      result = command['data']
    elif type_name == 'Variable':
      result = command['data'][1].replace('\r', '').replace('\n', '')
    elif type_name in ('int', 'Float'):
      result = command['data']
    elif type_name == 'str':
      result = '"{}"'.format(command['data'][1:-1])
    elif type_name == 'Object':
      result = self._cvt_go_object(command['data'], has_suffix = has_suffix)
    elif type_name == 'Function':
      result = self._cvt_go_func(command['data']) # has_suffix is unrelated to this func
    elif type_name == 'cont':
      result = self._cvt_go_cont(command['data']) # has_suffix is unrelated to this func
    elif type_name == 'EvokeCall':
      result = self._cvt_go_evokecall(command, has_suffix = has_suffix)
    elif type_name == 'DefineCall':
      result = self._cvt_go_definecall(command, has_suffix = has_suffix)
    else:
      result = '"Coa {} Object: {}"'.format(command['type'], command['data'])
    # result = '{}{}'.format(result, {True: '', False: ';'}[has_suffix])
    logging.debug('_cvt_go_object out {}'.format(str(result)))
    return result
  
  def _cvt_go_import(self):
    result = ''
    import_fmt = 'import "{}"\n'
    for i in range(len(self.cvt_go_lib)):
      lib = self.cvt_go_lib[i]
      result += import_fmt.format(lib)
    return result
  
  def _cvt_go(self, parsed):
    logging.debug('_cvt_go {}'.format(parsed))
    container = parsed[0]
    main_code = ''
    for i in range(len(container)):
      command = container[i]
      splitter = {'EvokeCall': self._cvt_go_evokecall, 'DefineCall': self._cvt_go_definecall,
                  'Object': self._cvt_go_object}
      main_code += str(splitter[command['type']](command, has_suffix = (len(command['suffix']) > 0)))
      for i in range(len(command['suffix'])):
        dotcommand = command['suffix'][i]
        main_code += str(splitter[dotcommand['type']](dotcommand, has_suffix = (i != len(command['suffix']) - 1)))
    import_code = self._cvt_go_import()
    return self.go_fmt.replace('<import_code>', import_code).replace('<main_code>', main_code)
  
  def cvt_go(self, parsed):
    logging.debug('cvt_go {}'.format(parsed))
    return self._cvt_go(parsed)
  
  def _itpt_evokecall(self, evokecall):
    logging.debug('_itpt_evokecall {}'.format(evokecall))
    result = [
      self._itpt_object(evokecall.obj),
      self._itpt_cont(evokecall.cont)
    ]
    return result
  
  def _itpt_definecall(self, definecall):
    logging.debug('_itpt_definecall {}'.format(definecall))
    result = [
      self._itpt_object(definecall.lhs),
      self._itpt_object(definecall.rhs)
    ]
    return result
  
  def _itpt_mtd(self, mtd):
    logging.debug('_itpt_mtd {}'.format(mtd))
    result = self._itpt_id(mtd.id)
    return result
  
  def _itpt_var(self, var):
    logging.debug('_itpt_var {}'.format(var))
    result = [
      var.dot,
      self._itpt_id(var.root),
    ]
    for i in range(len(var.ext)):
      mtd = var.ext[i]  # MeThoD
      result.append(self._itpt_mtd(mtd))
    return result
  
  def _itpt_func(self, func):
    logging.debug('_itpt_func {}'.format(func))
    result = [
      self._itpt_cont(func.args),
      self._itpt_cont(func.code)
    ]
    return result
  
  def _itpt_lpbk(self, obj):  # LooPBacK
    logging.debug('_itpt_lpbk {}'.format(obj))
    return obj
  
  def _itpt_bool(self, obj):
    logging.debug('_itpt_bool {}'.format(obj))
    return obj.bool
  
  def _itpt_float(self, obj):
    logging.debug('_itpt_float {}'.format(obj))
    return float('{}.{}'.format(obj.a, obj.b))
  
  def _itpt_id(self, obj):
    return obj.id
  
  def _itpt_object(self, command):
    logging.debug('_itpt_object {}'.format(command))
    obj = command.obj
    splitter = {'Bool': self._itpt_bool, 'int': self._itpt_lpbk, 'Float': self._itpt_float, 'str': self._itpt_lpbk,
                'Variable': self._itpt_var, 'Function': self._itpt_func, 'Id': self._itpt_id, 'NoneType': self._itpt_lpbk}
    result = {'type': obj.__class__.__name__, 'data': splitter[obj.__class__.__name__](obj)}
    return result
  
  def _itpt_enti(self, enti):
    logging.debug('_itpt_enti {}'.format(enti))
    command = enti.command
    ext = enti.ext
    splitter = {'EvokeCall': self._itpt_evokecall, 'DefineCall': self._itpt_definecall, 'Object': self._itpt_object}
    result = {'type': command.__class__.__name__, 'data': splitter[command.__class__.__name__](command), 'suffix': []}
    for i in range(len(ext)):
      dotcommand = ext[i].command
      result['suffix'].append({'type': dotcommand.__class__.__name__,
                               'data': splitter[dotcommand.__class__.__name__](dotcommand),
                               'suffix': None})
    return result
  
  def _itpt_cont(self, cont):
    logging.debug('_itpt_cont {}'.format(cont))
    entis = cont.entities
    result = []
    for i in range(len(entis)):
      enti = entis[i]
      result.append(self._itpt_enti(enti))
    return result
  
  def _itpt(self, model):
    logging.debug('_itpt {}'.format(model))
    conts = model.containers
    result = []
    for i in range(len(conts)):
      cont = conts[i]
      result.append(self._itpt_cont(cont))
    return result
  
  def itpt(self, model):
    logging.debug('itpt {}'.format(model))
    result = self._itpt(model)
    return result


def compile(in_p, out_p, lang = 'py',
            debug = False, textx_m_debug = None, textx_debug = None, itpt_debug = None,
            log_level = logging.DEBUG, log_path = 'coa-{}.log'.format(datetime.datetime.now().strftime('%Y-%m-%d %H-%M-%S'))):
  if not textx_m_debug: textx_m_debug = debug
  if not textx_debug:   textx_debug = debug
  if not itpt_debug:    itpt_debug = debug
  lang_config = {'go': {'grammar_p': './grammar.tx'}}
  if lang in lang_config.keys():
    mm = metamodel_from_file(lang_config[lang]['grammar_p'], debug = textx_m_debug)
    model = mm.model_from_file(in_p, debug = textx_debug)
    inst = program(log_level = log_level, log_path = log_path)
    parsed = inst.itpt(model)
    splitter = {'go': inst.cvt_go}
    source = splitter[lang](parsed)
    with open(out_p, 'w') as file:
      file.write(source)
    return parsed, source
  else:
    return ["error", "error"]
