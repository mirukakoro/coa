# Pod Names

Pod names are identifiers for pods that should be unique.

This pod name is for the `io.streams` core Coa pod.

```
dev.coa.core.io.streams
```

## Versions

Versions can be included in pod names.

```
dev.coa.core.io.streams('0.0.0-α')
```
