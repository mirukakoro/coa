# Pods (packages)

Coa "pods" are files (an archive file) that contain collections of Coa programs, like packages in other languages 
and containers (app containerization). Coa pods are meant to be small, and 
access other pods for additional functionality. A good example of this is 
Flask, where there is basic functionality in the `Flask` package, but has 
plugins such as `Flask-Admin` to add extra functionality.

Each pod should be structured as follows. Objects starting with `*` are optional. `.md` files can also be other file formats (e.g. `.rst`, `.txt`, `.html`)

```
server.pod
├─license.md
├─readme.md
├─info.cod
├─docs
| ├─index.md
| ├─api_reference.md
| ├─pros_and_cons.md
| ├─tutorials.md
| └─notes.md
├─source
| ├─main.coa
| ├─dispatcher.coa
| ├─handler.coa
| └─manager.coa
```