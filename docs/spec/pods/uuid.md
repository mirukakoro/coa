# Pod UUID

Pod UUIDs are identifiers of pods that should not be used by default. These should be unique.

```
cbea70d6-599e-4826-b0c9-d8ea4f21467d
```
