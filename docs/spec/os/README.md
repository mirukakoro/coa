# Coa OS Spec

## Filesystem

This example is for a Coa OS System with name `server-0` with a example proxy server.

```
/
└─pods/
  ├─local.server-0.context/
  ├─local.server-0.context.network/
  ├─local.server-0.context.hard/
  ├─local.server-0.context.soft/
  ├─dev.coa.os.core/
  ├─dev.coa.os.core.overseer/
  ├─dev.coa.os.core.file/
  ├─dev.coa.os.core.file.history/
  ├─dev.coa.os.core.file.overseer/
  ├─dev.coa.os.core.contact/
  ├─dev.coa.os.core.contact.linux/
  ├─dev.coa.os.core.contact.linux.alpine/
  ├─dev.coa.os.core.comm.server/
  ├─dev.coa.os.core.comm.client/
  ├─dev.coa.os.core.comm.peer/
  ├─dev.coa.core/
  ├─dev.coa.core.overseer/
  ├─dev.coa.core.io/
  ├─dev.coa.core.io.file/
  ├─dev.coa.core.io.comm/
  ├─dev.coa.core.io.contact/
  ├─dev.coa.core.math/
  ├─dev.coa.core.math.simple/
  ├─dev.coa.core.math.matrix/
  ├─dev.coa.core.object/
  ├─dev.coa.lang/
  ├─dev.coa.lang.overseer/
  ├─dev.coa.lang.go/
  ├─dev.coa.lang.go.yaegi/
  ├─dev.coa.lang.go.golang/
  ├─dev.coa.lang.go.parser/
  ├─dev.coa.lang.py.parser/
  ├─dev.coa.idx.proxy/
  ├─dev.coa.idx.proxy.overseer/
  ├─dev.coa.idx.proxy.handler/
  └─dev.coa.idx.proxy.requester/
```

`dev.coa.os.core.file.overseer.pod` oversees all other pods except `dev.coa.os.core.pod`.
