# Coa Spec

## Pods (packages)

Coa "pods" are collections of Coa programs, like packages in other languages 
and containers (app containerization). Coa pods are meant to be small, and 
access other pods for additional functionality. A good example of this is 
Flask, where there is basic functionality in the `Flask` package, but has 
plugins such as `Flask-Admin` to add extra functionality.

Each pod should be structured as follows. Objects starting with `*` are optional.

```
server
├─*LICENSE
├─*license
├─*README.md
├─*readme.md
├─info
| ├─main.cod
| ├─permits.cod
| ├─dependencies.cod
| ├─notes.cod
| ├─legal.cod
| └─readme.md
├─*docs
| ├─index.md
| ├─api_reference.md
| ├─pros_and_cons.md
| ├─tutorials.md
| └─notes.md
├─source
| ├─main.coa
| ├─dispatcher.coa
| ├─handler.coa
| └─manager.coa
```

## Language

### Find the prime numbers in 0~999 (inclusive)

```coa
(
  .class(.int,
    ().(
      .super(.args, ),
    ),
    ().(
      .range(i, .this.**(0.5).+(1)).for((i).(
        .then.%(i).!=(0).then(
          .return(.false, ),
        ), ),
      ),
      .return(.true, ),
    ).put(is_prime),
  ).put(.int, ),
  .list().put(primes, ),
  1000.range().for((i, ).(
    i.is_prime.then(().(
      primes.+(i),
    ), ),
  ), ),
  list.length.==(1).then(
    .out('There was "primes.length" prime number:", ),
  ).not().then(
    .out('There were "primes.length" prime numbers:", ), 
  ),
  list.length.range().for((i).(
    .out('"i.string.fill(3, )" "list(i)"', ),
  ), ),
)
```
