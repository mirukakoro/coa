# [OLD] Coa Spec

![Page | Old](https://img.shields.io/badge/Page-Old-orange?style=flat-square)

## *Program*

Every program may start with `(` and end with `)`. It can contain zero or more *Command*.
```
(
  *Command*
)
```

## *Command*

Every command is a EvokeCall or DefineCall or Object. May have a `,` at the end.

## *EvokeCall*

*EvokeCall* evokes a *Function* object with required and optional argument(s).

## *DefineCall*

*DefineCall* defines an object (*Object*) to a variable (*Id*).

## *Object*

*Object* is a entity that takes up memory. When compiled to Go, is becomes a native Go entity (usually, depends on type).
