module.exports = {
  title: 'Coa',
  description: 'Clean, objective, asynchronous.',
  base: '/coa/',
  dest: 'public',
  markdown: {
    lineNumbers: true
  },
  themeConfig: {
    repo: 'https://gitlab.com/colourdelete/coa',
    smoothScroll: true,
    repoLabel: 'GitLab',
    docsRepo: 'colourdelete/coa',
    docsDir: 'docs',
    docsBranch: 'master',
    editLinks: true,
    editLinkText: 'Edit on GitLab'
  },
};
