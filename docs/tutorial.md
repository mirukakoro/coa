# Tutorial

Inspired by [The Python Tutorial](https://docs.python.org/3/tutorial).

Coa is a clean and multi-paradigm programming language that supports easy asynchronization. Coa has high-level object structures and a simple approach to object-oriented programming. Coa's clean syntax and semi-compiled nature makes it an ideal language for development in most environments.

## Why Coa?

When you do stuff on computers, you'll find tasks that you want/need to automate. For example, you might want to replace text in a large number of files/data, or make a GUI for your local NAS, or track your inventory using a database.

You might be using other languages such like Java, C, C++, C#, or Swift but they may take some time to compile (C++), or may be too verbose (Java) and hard to learn (C++). Coa is your companion.

Coa is semi-compiled into [Go](https://golang.org), which either compiles or interprets it. Because of this, it can be both compiled into binary files with ease and run on-the-fly with a Go interpreter. This makes it easy to write one-file programs to automate a one-time task, such as the number of factors of 12345678 (24).

Coa programs can be written consicely and readably. This is for several reasons:

- The high-level objects allow complex expressions in a single "statement"
- No object declarations are necessary (a variable can't "not exist", so you can't get something like a `NameError`)
- Putting together commands are easier

Coa programs should never be long. They are best when they are short, and extending using pods (packages) and other files is encouraged.

## How to Start

A good place to start is by writing a small program that ensures that all necessary entities are working by evoking `coa status`, `coa doctor`, or `coa diagnosis`. If you see an output similar to the one below, the environment is fine.

```
$ coa status
Coa 1.0.0 δ
[✓] Coa 1.0.0 δ
  [✓] Compiler (dev.coa.compiler)
  [✓] Path Config (/opt/coa)
  [✓] Doctor
  [✓] Coa Pod 1.0.0 δ (dev.coa.pod)
[✓] Go 1.14.3
  [✓] Go (/usr/local/go)
  [✓] Compiler (/usr/local/go)
  [✓] Interpreter (yaegi)
[✓] IDE (0 IDEs found)
```

Let's write our (first) Coa program!

Write the following text into a file named `first-program.coa`

```
.out('This is Coa ".coa.version" ".coa.channel". ', ),
```

Use `coa run` or `coa build` to run or build the Coa program. `coa run` defaults to a interpreter if found.

::: warning
The output shown below is a snapshot of the output. When running on a terminal, the big dots will move around like a progress bar.
:::

Using `coa run`:

```
$ coa run first-program.coa
●∙∙ ∙∙∙ ∙∙∙ Parsing      from Coa
```

::: tip
You might see `●●● ∙●∙ ∙●∙ Interpreting from Coa` if a Coa interpreter is available.
:::

```
$ coa run first-program.coa
●●● ∙●∙ ∙∙∙ Compiling    from Coa
```

```
$ coa run first-program.coa
●●● ●●● ∙∙● Interpreting from Go
```

```
$ coa run first-program.coa
●●● ●●● ●●● Compiled in 1.978 s
This is Coa 1.0.0 δ.
```

Using `coa build`:

```
$ coa build first-program.coa
●∙∙ ∙∙∙ ∙∙∙ Parsing      from Coa
```

::: tip
You might see `●●● ∙●∙ ∙●∙ Compiling    from Coa` if a Coa compiler that compiles to binary/assembly is available.
:::

```
$ coa build first-program.coa
●●● ∙●∙ ∙∙∙ Compiling    from Coa
```

```
$ coa build first-program.coa
●●● ●●● ∙∙● Compiling    from Go
```

```
$ coa build first-program.coa
●●● ●●● ●●● Compiled in 1.978 s
$ first-program
This is Coa 1.0.0 δ.
```

::: tip
Even though the tutorial portion above shows multiple snapshots, subsequent portions would not, expect when showing features shown best with time unless known/mentioned already in a previous/current portion.
:::

Here's an explanation of the above Coa program.

The first function in our program, `.out` is similar to a `print` or `PrintLn` function. It takes a object and writes the string representation to stdout. Also, there is `.in` and `.err` for reading and writing from and to stdin and stderr.

::: tip
Even though there are aliases such as `.print` or `.println`, `.io.out` or `.out` is preferred due to clarity or conciseness.
:::

The first argument inside the first `.out` evoke is a text, `'This is Coa ".coa.version" ".coa.channel". '`, which embeds the Coa version and Coa channel into the text and prints it out to stdout. `.coa.version` returns the version of Coa that was/is compiled against, or the version of Coa that was/is interpreted against. `.coa.channel` returns the channel of Coa (same as above for specifics).

Coa programs can have embeddings in programs using `"` and enclosing objects inside them. This can be done in text, as well as program source. Here's an example:

```
.range(0, 99, ).for(
    (i, ).(
        var-"i" = "i",
    ),
),
.out(var-0, ),
.out(var-44, ),
.out(var-99, ),
```
```
0
44
99
```

In this program, Coa defines 100 variables from `var-0` to `var-99`, and prints them out. As you can see, instead of hard-coding them, we made 100 separate variables using a for loop (`var-0~99`)!
