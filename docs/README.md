---
home: true
heroImage: /icon.svg
features:
- title: Clean
  details: Coa allows writing complex code with only using minimal types of characters.
- title: Objective
  details: 
- title: Asynchronous
  details: Coa is designed to be easy to code asynchronously.
footer: Coa by Colourdelete Website may contain cookies used by Gitlab. Since this website itself (https://colourdelete.gitlab.io/coa) is static, it does not and usually cannot collect of se cookies. However, gitlab.com or GitLab might use and ccollect cookies.
---

![Pipeline Status](https://gitlab.com/colourdelete/coa/badges/master/pipeline.svg?style=flat-square "Pipeline Status")

## What is Coa?

> Coa is a **clean** and **multi-paradigm** programming language that supports **easy asynchronization**.

## Coa is simple and easy.

```coa
.class(
  ( length, ).(),
  ().( .return( 'A chain with "length" links', ), ).=( str, ),
).=( chain, ),
```

```
class chain():
  def __init__(self, length):
    self.length = length
  def __str__(self):
    return 'A chain with {} link(s)'.format(self.length)
```

<!-- <table>
  <tr>
    <th>
      Coa
    </th>
    <td>
      ```
      .class(

        ( length, ).(
          length.=( .i.length),
        ),

        ().(
          .return( 'A chain with ".i.length" links'), 
        ).=( .i.str(), ),

      ).=( chain, ),
      ```
    </td>
  </tr>
  <tr>
    <th>
      Python
    </th>
    <td>
    ```
    class chain():
      def __init__(self, length):
        self.length = length

      def __str__(self):
        return 'A chain with {} link(s)'.format(self.length)
    ```
    </td>
  </tr>
</table> -->

[[toc]]

<!-- ### Coa

```coa
(
  .class(.object,
  (names, up, down, ).(
    names.required(),
    names(0,   ).=(.this.first_name  ),
    names(-1,  ).=(.this.last_name   ),
    names(1, -2, ).=(.this.middle_names),
    up  .=(.this.up  ),
    down.=(.this.down),
  ),
  ().(
    .return('".this.first_name" ".this.middle_names.string(' '.=(sep, ), )" ".this.last_name"')
  ).=(.this.string)
  ).=(person)
  person(.list('Jacy', 'Doe'), ).=(jacy),
  person(.list('Jaden', 'Doe'), ).=(jaden),
  person(.list('Jackie', 'Jacy', 'Jaden', 'Doe'), list(jacy.await, jaden.await, ), ).=(jackie),
  jacy.await.down.=(jackie),
  jaden.await.down.=(jackie),
  .out(jacy.await),
  .out(jaden.await),
  .out(jackie.await),
)
```
```
Jacy Doe
Jaden Doe
Jackie Jacy Jaden Doe
```

### Python

```python
class person():
  def __init__(self, names, up=None, down=None):
  self.first_name = names[0]
  self.middle_names = names[1:-1]
  self.last_name = names[-1]
  self.up = up
  self.down = down
  def __str__():
  result = self.first_name + ' '
  for middle_name in self.middle_names:
    result += middle_name + ' '
  result += self.last_name
  return result
jacy = person(['Jacy', 'Doe'])
jaden = person(['Jaden', 'Doe'])
jackie = person(['Jackie', 'Doe'], up=[jacy, jaden])
jacy.down = [jackie]
jaden.down = [jackie]
print(jacy)
print(jaden)
print(jackie)
```
```
Jacy Doe
Jaden Doe
Jackie Jacy Jaden Doe
``` -->

## How does Coa work?

A compiler converts Coa code to another programming language (Go for the default), which in turn compiles it to machine language. Some features may be unavailable or slow depending on the compiler. This compiler (this repo) converts Coa code into Go code using a Python program. It does not run commands such as `go run` or `go build` as of March 2020.

If the graph below doesnt appear, use [a copy on GitLab]https://gitlab.com/colourdelete/coa/-/blob/master/docs/README.md).

```mermaid
graph TD
  A(Coa Program) -->|"Coa Compiler - Compile Coa🡲Go"| B(Go Program)
  B -->|"Go Compiler - Compile Go🡲Executable"| C(Executable Program)
```
