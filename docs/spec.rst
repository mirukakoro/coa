Coa Spec
========

*Program*
---------

Every program may start with ``(`` and end with ``)``. It can contain
zero or more *Command*.

::

   (
     *Command*
   )

*Command*
---------

Every command is a *EvokeCall* or *DefineCall* or *Object*. May have a
``?`` at the end.