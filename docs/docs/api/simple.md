# Simple Object

[[ toc ]]

## `.object`

::: tip
Even though `object`s are simple, that doesn't mean that its inheritors are!
:::

### `.object()`

#### Patterns

* `.object()`

#### Raises

Raises no exceptions.

::: warning
Even though `object`s raises no exceptions on its own, some exceptions such as `.coa.exceptions.memory.unavailable` or `.coa.exceptions.permits.unpermitted`.
:::

#### Description

The root object. All other object are inheitors of `.object`.

::: tip
Even if `.class` is used without an inheritee, it still uses `.object` as it's inheritee.
(Inheritee is not a real word, but it is used to describe who the inheritor inherited from.)
:::

### `.object.=()`

#### Patterns

* `object.=(object_2, )`

#### Raises

* `.object.exceptions.unusable` when `object..name` and `object_2..name` are both `.none`

#### Description

Changes `object` or `object_2` to have the contents of `object_2` or `object`.

* If `object..name` is `.none` and `object_2..name` is not `.none`, `object`'s contents clones `object_2` contents.
* If `object_2..name` is `.none` and `object..name` is not `.none`, `object_2`'s contents clones `object` contents.
* If `object_2..name` is `.none` and `object..name` is `.none`, `object.=` raises `.object.exceptions.unusable`.
* If `object_2..name` is not `.none` and `object..name` is not `.none`, `object_2`'s contents clones `object` contents.

::: danger
This is a dangerous warning
:::

::: details
This is a details block, which does not work in IE / Edge
:::