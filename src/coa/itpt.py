from textx import metamodel_from_file
import json
import logging
import datetime

__version__ = '0.0.0-a'


class program():
  def __init__(self, log_level=logging.DEBUG,
               log_path='coa-{}.log'.format(datetime.datetime.now().strftime('%Y-%m-%d %H-%M-%S'))):
    self.config = {}
    with open('config.json') as file:
      self.config = json.load(file)
    with open('template.go') as file:
      self.go_fmt = file.read().replace('<ver>', __version__).replace('<pkg_name>', 'main')
    self.callee_conv = self.config['cvt_go']['callee']
    self.definecall_hst = []
    self.cvt_go_lib = []
    self.cvt_go_import_fmt = 'import "{}"'
    # log_path = None
    logging.basicConfig(
      format = '%(asctime)s %(levelname)-8s %(message)s',
      level = log_level,
      filename = log_path,
      datefmt = '%Y-%m-%d %H:%M:%S (%z)')
    logging.info('Init program class.')
  
  def _itpt_evokecall(self, evokecall):
    logging.debug('_itpt_evokecall {}'.format(evokecall))
    result = {
      'type': 'EvokeCall',
      'data': [
        self._itpt_object(evokecall.obj),
        self._itpt_cont(evokecall.cont),
      ]
    }
    return result
  
  def _itpt_mtd(self, mtd):
    logging.debug('_itpt_mtd {}'.format(mtd))
    result = self._itpt_id(mtd.id)
    return result
  
  def _itpt_var(self, var):
    logging.debug('_itpt_var {}'.format(var))
    result = [
      var.dot,
      self._itpt_id(var.root),
    ]
    for i in range(len(var.ext)):
      mtd = var.ext[i]  # MeThoD
      result.append(self._itpt_mtd(mtd))
    return result
  
  def _itpt_func(self, func):
    logging.debug('_itpt_func {}'.format(func))
    result = [
      self._itpt_cont(func.args),
      self._itpt_cont(func.code)
    ]
    return result
  
  def _itpt_lpbk(self, obj):  # LooPBacK
    logging.debug('_itpt_lpbk {}'.format(obj))
    return obj
  
  def _itpt_bool(self, obj):
    logging.debug('_itpt_bool {}'.format(obj))
    return obj.bool
  
  def _itpt_float(self, obj):
    logging.debug('_itpt_float {}'.format(obj))
    return float('{}.{}'.format(obj.a, obj.b))
  
  def _itpt_id(self, obj):
    logging.debug('_itpt_id {}'.format(obj))
    return obj.id
  
  def _itpt_str(self, obj):
    logging.debug('_itpt_str {}'.format(obj))
    obj['type'] = 'String'
    return obj
  
  def _itpt_object(self, command):
    logging.debug('_itpt_object {}'.format(command))
    obj = command.obj
    splitter = {'Bool': self._itpt_bool, 'int': self._itpt_lpbk, 'Float': self._itpt_float, 'str': self._itpt_lpbk,
                'Variable': self._itpt_var, 'Function': self._itpt_func, 'EvokeCall': self._itpt_evokecall,
                'Object': self._itpt_object, 'NoneType': self._itpt_lpbk}
    result = splitter[obj.__class__.__name__](obj)
    return result
  
  def _itpt_enti(self, enti):
    logging.debug('_itpt_enti {}'.format(enti))
    command = enti
    ext = enti.ext
    # result = {'type': command.__class__.__name__, 'data': self._itpt_object(command), 'suffix': None}
    result = self._itpt_object(command)
    # if len(ext) > 0:
    #   DotCommand = ext[0]
    #   if len(ext) == 1:
    #     result['suffix'] = {'type': DotCommand.__class__.__name__,
    #                         'data': self._itpt_object(DotCommand),
    #                         'suffix': None}
    #   elif len(ext) > 1:
    #     result['suffix'] = {'type': DotCommand.__class__.__name__,
    #                         'data': self._itpt_object(DotCommand),
    #                         'suffix': ext[1:]}
    # else:
    #   result['suffix'] = None
    logging.warning('suffix of itpt enti {}'.format(result))
    if len(ext) > 0:
      DotCommand = ext[0]
      if len(ext) == 1:
        result['suffix'] = self._itpt_object(DotCommand)
      elif len(ext) > 1:
        result['suffix'] = self._itpt_object(DotCommand)
    # else:
    #   result['suffix'] = None
    return result
  
  def _itpt_cont(self, cont):
    logging.debug('_itpt_cont {}'.format(cont))
    entis = cont.entities
    result = []
    for i in range(len(entis)):
      enti = entis[i]
      result.append(self._itpt_enti(enti))
    return result
  
  def _itpt(self, model):
    logging.debug('_itpt {}'.format(model))
    conts = model.containers
    result = []
    for i in range(len(conts)):
      cont = conts[i]
      result.append(self._itpt_cont(cont))
    if len(result) == 1:
      result = result[0]
    return result
  
  def itpt(self, model):
    logging.debug('itpt {}'.format(model))
    result = self._itpt(model)
    return result
