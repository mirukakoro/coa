from textx import metamodel_from_file
import json
import logging
import datetime

__version__ = '0.0.0-a'


class go_program():
  def __init__(self, log_level=logging.DEBUG,
               log_path='coa-{}.log'.format(datetime.datetime.now().strftime('%Y-%m-%d %H-%M-%S'))):
    self.config = {}
    with open('config.json') as file:
      self.config = json.load(file)
    with open('template.go') as file:
      self.go_fmt = file.read().replace('<ver>', __version__).replace('<pkg_name>', 'main')
    self.callee_conv = self.config['cvt_go']['callee']
    self.definecall_hst = []
    self.cvt_go_lib = ['fmt']
    self.cvt_go_import_fmt = 'import "{}"\n'
    # log_path = None
    logging.basicConfig(
      format = '%(asctime)s %(levelname)-8s %(message)s',
      level = log_level,
      filename = log_path,
      datefmt = '%Y-%m-%d %H:%M:%S (%z)')
    logging.info('Init program class.')
  
  def _cvt_go_gen_arr(self, arr):
    arr_fmt    = '[{}]string{}'
    content    = '{'
    for i in range(len(arr)):
      content += '"{}"'.format(arr[i])
      if i != len(arr)-1:
        content += ','
    content   += '}'
    return arr_fmt.format(len(arr), content)
  
  def _cvt_go_evokecall(self, evokecall):
    logging.debug('go_program._cvt_go_evokecall {}'.format(evokecall))
    raw_callee = evokecall['data'][0]
    args       = evokecall['data'][1]
    if raw_callee[0] == '.':
      callee   = ['dev', 'coa', 'core', ]
    else:
      callee   = []
    for i in range(len(raw_callee)):
      if raw_callee[i] != '.':
        callee.append(raw_callee[i])
    fmt        = 'CoaMakeEvokeCall({}, {});\n'
    return fmt.format(self._cvt_go_gen_arr(callee), '"wip"')
  
  def _cvt_go_variable(self, variable):
    fmt = 'CoaMakeVariable("{}", {});\n'
    return fmt.format('wip')
  
  def _cvt_go_function(self, function):
    fmt = 'CoaMakeFunction({}, {});\n'
    return fmt.format('wip')
  
  def _cvt_go_import_code(self):
    result = ''
    for i in range(len(self.cvt_go_lib)):
      result += self.cvt_go_import_fmt.format(self.cvt_go_lib[i])
    return result
  
  def _cvt_go(self, commands, pkg_name, template_path, in_p, out_p):
    splitter = {
      'EvokeCall': self._cvt_go_evokecall,
      'Variable': self._cvt_go_variable,
    }
    result = ''
    logging.debug('commands is {}'.format(commands))
    for i in range(len(commands)):
      command = commands[i]
      result += splitter[command['type']](command)
    with open(template_path, 'r') as file:
      template = file.read()
    template = template.replace('<ver>', __version__)
    template = template.replace('<in_path>', in_p)
    template = template.replace('<out_path>', out_p)
    template = template.replace('<pkg_name>', pkg_name)
    template = template.replace('<import_code>', self._cvt_go_import_code())
    template = template.replace('<main_code>', result)
    return template
  
  def cvt_go(self, data, pkg_name='main', template_path='template.go',
             in_p='unknown input path', out_p='unknown output path'):
    return self._cvt_go(data, pkg_name=pkg_name, template_path=template_path, in_p=in_p, out_p=out_p)